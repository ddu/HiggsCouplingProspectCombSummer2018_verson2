skipHmumu_mu=0
skipHZZ_mu=1
skipHZZ_mu_noBR=1
skipHZZ_XS=1
skipHZZ_XS_noBR=1
skipHtautau_mu=1
skipHtautau_mu_noBR=1
skipHtautau_XS=1
skipHtautau_XS_noBR=1
skipHWW_mu=1
skipHWW_mu_noBR=1
skipHWW_XS=1
skipHWW_XS_noBR=1
skipHGamma_mu=1
skipHGamma_mu_noBR=1
skipHGamma_XS=1
skipHGamma_XS_noBR=1
skipttHbb_dilep_mu=1
skipttHbb_dilep_mu_noBR=1
skipttHbb_dilep_XS=1
skipttHbb_dilep_XS_noBR=1
skipttHbbljets_mu=1
skipttHbbljets_mu_noBR=1
skipttHbbljets_XS=1
skipttHbbljets_XS_noBR=1
skipttHMLnotau_mu=1
skipttHMLnotau_mu_noBR=1
skipttHMLnotau_XS=1
skipttHMLnotau_XS_noBR=1
skipttHMLwithTau_mu=1
skipttHMLwithTau_mu_noBR=1
skipttHMLwithTau_XS=1
skipttHMLwithTau_XS_noBR=1
skipVHbb_mu=1
skipVHbb_mu_noBR=1
skipVHbb_XS=1
skipVHbb_XS_noBR=1
mkdir -vp HiggsCouplingProspectCombSummer2018/workspaces/v6/WS_3000/reparam_input
file=HiggsCouplingProspectCombSummer2018/workspaces/v6/WS_3000/reparam_input
##################Hmumu workspace######################################
if [ -f "$file"/"WS-Hmumu-mu.root" -a $skipHZZ_mu = 1 ]; then
    echo "Hmumu mu workspace exist(skip)"
else
    manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_Hmumu.xml  --wsName uuspace -d AsimovSB
fi

################### HZZ workspace#######################################
if [ -f "$file"/"WS-HZZ-mu_80ifb.root" -a $skipHZZ_mu = 1 ]; then
    echo "HZZ mu workspace exist(skip)"
else
    manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_HZZ.xml  --wsName combined -d asimovData
fi

if [  -f "$file"/"WS-HZZ-mu_noBR_80ifb.root" -a $skipHZZ_mu_noBR = 1 ]; then
    echo "HZZ mu_noBR workspace exist(skip)"
else
    manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_HZZ_mu_noBR.xml  --wsName combined -d asimovData
fi

if [ -f "$file"/"WS-HZZ-xs_80ifb.root" -a $skipHZZ_XS = 1 ]; then
    echo "HZZ xs workspace exist(skip)"
else
    manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_HZZ_xs.xml  --wsName combined -d asimovData
fi

if [  -f "$file"/"WS-HZZ-xs_noBR_80ifb.root" -a $skipHtautau_XS_noBR = 1 ]; then
    echo "HZZ xs_noBR workspace exist(skip)"
else
    manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_HZZ_xs_noBR.xml  --wsName combined -d asimovData
fi 
###################Htautau workspace####################################
if [ -f "$file"/"WS-Htautau-mu.root" -a $skipHtautau_mu = 1 ]; then
    echo "Htautau mu workspace exist(skip)"
else
    manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_Htautau.xml  --wsName combined -d asimovData
fi

if [  -f "$file"/"WS-Htautau-mu_noBR.root" -a $skipHtautau_mu_noBR = 1 ]; then
    echo "Htautau mu_noBR workspace exist(skip)"
else
    manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_Htautau_mu_noBR.xml  --wsName combined -d asimovData
fi
if [ -f "$file"/"WS-Htautau-xs.root" -a $skipHtautau_XS = 1 ]; then
    echo "Htautau xs workspace exist(skip)"
else
    manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_Htautau_xs.xml  --wsName combined -d asimovData
fi

if [  -f "$file"/"WS-Htautau-xs_noBR.root" -a $skipHtautau_XS_noBR = 1 ]; then
    echo "Htautau xs_noBR workspace exist(skip)"
else
    manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_Htautau_xs_noBR.xml  --wsName combined -d asimovData
fi

################HWW workspace##########################################

if [  -f "$file"/"WS-HWW-mu.root" -a $skipHWW_mu = 1 ]; then
    echo "HWW mu workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_HWW.xml  --wsName HWWRun2 -d asimovData_1
fi

if [  -f "$file"/"WS-HWW-mu_noBR_lumi_split.root" -a $skipHWW_mu_noBR = 1 ]; then
    echo "HWW mu_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_HWW_mu_noBR.xml  --wsName HWWRun2 -d asimovData_1
fi

if [  -f "$file"/"WS-HWW-xs.root" -a $skipHWW_XS = 1 ]; then
    echo "HWW XS workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_HWW_xs.xml  --wsName HWWRun2 -d asimovData_1
fi

if [  -f "$file"/"WS-HWW-xs_noBR.root" -a $skipHWW_XS_noBR = 1 ]; then
    echo "HWW XS_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_HWW_xs_noBR.xml  --wsName HWWRun2 -d asimovData_1
fi
##############Hgamma gamma workspace#######################################
if [  -f "$file"/"WS-HGam-xs_80ifb.root" -a $skipHGamma_XS = 1 ]; then
    echo "HGamma XS workspace exist(skip)"
else
    manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_HGam_xs.xml  --wsName combWS -d obsData --mcName mconfig
fi

if [  -f "$file"/"WS-HGam-xs_noBR_80ifb.root" -a $skipHGamma_XS_noBR = 1 ]; then
    echo "HGamma XS_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_HGam_xs_noBR.xml  --wsName combWS -d obsData --mcName mconfig
fi

if [  -f "$file"/"WS-HGam-mu_80ifb.root" -a $skipHGamma_mu = 1 ]; then
    echo "HGamma mu workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_HGam.xml  --wsName combWS -d obsData --mcName mconfig
fi

if [  -f "$file"/"WS-HGam-mu_noBR_80ifb.root" -a $skipHGamma_mu_noBR = 1 ]; then
    echo "HGamma mu_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_HGam_noBR.xml  --wsName combWS -d obsData --mcName mconfig
fi
####################ttHbb_dilep workspace################################
if [ -f "$file"/"WS-ttH-bbdil-mu_noBR_lumi_split.root" -a $skipttHbb_dilep_mu_noBR = 1 ]; then
    echo "ttH-bbdil mu_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHbbdil_mu_noBR.xml  --wsName combined -d asimovData_1
fi

if [  -f "$file"/"WS-ttH-bbdil-mu.root" -a $skipttHbb_dilep_mu = 1 ]; then
    echo "ttH-bbdil mu workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHbbdil.xml  --wsName combined -d asimovData_1
fi

if [  -f "$file"/"WS-ttH-bbdil-xs.root" -a $skipttHbb_dilep_XS = 1 ]; then
    echo "ttH-bbdilep XS workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHbbdil_xs.xml  --wsName combined -d asimovData_1
fi

if [  -f "$file"/"WS-ttH-bbdil-xs_noBR.root" -a $skipttHbb_dilep_XS_noBR = 1 ]; then
    echo "ttH-bbdilep XS_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHbbdil_xs_noBR.xml  --wsName combined -d asimovData_1
fi
#######################ttHbbljets workspace#########################################
if [  -f "$file"/"WS-ttH-bbljets-mu.root" -a $skipttHbbljets_mu = 1 ]; then
    echo "ttH-bbljets mu workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHbbljets.xml  --wsName combined -d asimovData_1
fi

if [  -f "$file"/"WS-ttH-bbljets-mu_noBR_lumi_split.root" -a $skipttHbbljets_mu_noBR = 1 ]; then
    echo "ttH-bbljets mu_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHbbljets_mu_noBR.xml  --wsName combined -d asimovData_1
fi

if [  -f "$file"/"WS-ttH-bbljets-xs.root" -a $skipttHbbljets_XS = 1 ]; then
    echo "ttH-bbljets XS workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHbbljets_xs.xml  --wsName combined -d asimovData_1
fi

if [  -f "$file"/"WS-ttH-bbljets-xs_noBR.root" -a $skipttHbbljets_XS_noBR = 1 ]; then
    echo "ttH-bbljets XS_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHbbljets_xs_noBR.xml  --wsName combined -d asimovData_1
fi
######################ttHMLnotau workspace############################################
if [  -f "$file"/"WS-ttH-MLnotau-mu.root" -a $skipttHMLnotau_mu = 1 ]; then
    echo "ttH-ML_noTau mu workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHMLnotau.xml  --wsName combined -d asimovData_1
fi

if [  -f "$file"/"WS-ttH-MLnotau-mu_noBR.root" -a $skipttHMLnotau_mu_noBR = 1 ]; then
    echo "ttH-ML_noTau mu_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHMLnotau_mu_noBR.xml  --wsName combined -d asimovData_1
fi

if [  -f "$file"/"WS-ttH-MLnotau-xs.root" -a $skipttHMLnotau_XS = 1 ]; then
    echo "ttH-ML_noTau XS workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHMLnotau_xs.xml  --wsName combined -d asimovData_1
fi

if [  -f "$file"/"WS-ttH-MLnotau-xs_noBR.root" -a $skipttHMLnotau_XS_noBR = 1 ]; then
    echo "ttH-ML_noTau XS_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHMLnotau_xs_noBR.xml  --wsName combined -d asimovData_1
fi
#########################ttHMLwithTau workspace#####################################
if [  -f "$file"/"WS-ttH-MLtau-mu.root" -a $skipttHMLwithTau_mu = 1 ]; then
    echo "ttH-ML_tau mu workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHMLtau.xml  --wsName combined -d asimovData_1
fi

if [  -f "$file"/"WS-ttH-MLtau-mu_noBR_lumi_split.root" -a $skipttHMLwithTau_mu_noBR = 1 ]; then
    echo "ttH-ML_tau mu_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHMLtau_mu_noBR.xml  --wsName combined -d asimovData_1
fi

if [  -f "$file"/"WS-ttH-MLtau-xs.root" -a $skipttHMLwithTau_XS = 1 ]; then
    echo "ttH-ML_Tau XS workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHMLtau_xs.xml  --wsName combined -d asimovData_1
fi

if [  -f "$file"/"WS-ttH-MLtau-xs_noBR.root" -a $skipttHMLwithTau_XS_noBR = 1 ]; then
    echo "ttH-ML_Tau XS_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_ttHMLtau_xs_noBR.xml  --wsName combined -d asimovData_1
fi
##########################################VHbb workspace#############################################
if [  -f "$file"/"WS-VHbb.root" -a $skipVHbb_mu = 1 ]; then
    echo "VHbb mu workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_VHbb.xml  --wsName combined -d asimovData
fi

if [  -f "$file"/"WS-VHbb-mu_noBR.root" -a $skipVHbb_mu_noBR = 1 ]; then
    echo "VHbb mu_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_VHbb_noBR.xml  --wsName combined -d asimovData
fi

if [  -f "$file"/"WS-VHbb-xs.root" -a $skipVHbb_XS = 1 ]; then
    echo "VHbb XS workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_VHbb_xs.xml  --wsName combined -d asimovData
fi

if [  -f "$file"/"WS-VHbb-xs_noBR.root" -a $skipVHbb_XS_noBR = 1 ]; then
    echo "VHbb XS_noBR workspace exist(skip)"
else
	manager -w organize -x /afs/cern.ch/work/d/ddu/workspaceCombiner/workspaceCombiner/cfg/fcnc/CombinedXML/v6/WS_3000/modification/modify_VHbb_xs_noBR.xml  --wsName combined -d asimovData
fi
