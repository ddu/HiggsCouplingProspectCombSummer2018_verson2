#ifndef ROOT_RooDSCBShape
#define ROOT_RooDSCBShape

#include <math.h>
#include "Math/ProbFuncMathCore.h"
#include "Riostream.h"
#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooFit.h"
#include "RooMath.h"
#include "RooRealProxy.h"
#include "RooRealVar.h"
#include "TMath.h"

class RooRealVar;

class RooDSCBShape : public RooAbsPdf {
  
 public:
  
  RooDSCBShape();
  RooDSCBShape(const char *name, const char *title, RooAbsReal& _m,
		   RooAbsReal& _m0, RooAbsReal& _sigma, RooAbsReal& _alphaLo,
		   RooAbsReal& _nLo, RooAbsReal& _alphaHi, RooAbsReal& _nHi);
  
  RooDSCBShape(const RooDSCBShape& other, const char* name = 0);
  virtual TObject* clone(const char* newname) const { return new RooDSCBShape(*this,newname); }
  inline virtual ~RooDSCBShape() { }
  
  //virtual Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars,
  //				      const char* rangeName=0 ) const;
  //virtual Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const;
  Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars,
			      const char* rangeName=0) const;
  Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const;
  
  double gaussianIntegral(double tmin, double tmax) const;
  double powerLawIntegral(double tmin, double tmax, double alpha, double n) const;
  
 protected:
  
  RooRealProxy m;
  RooRealProxy m0;
  RooRealProxy sigma;
  RooRealProxy alphaLo;
  RooRealProxy nLo;
  RooRealProxy alphaHi;
  RooRealProxy nHi;
  
  Double_t evaluate() const;
  
 private:
  
  ClassDef(RooDSCBShape,1); // Crystal Ball lineshape PDF
    
};
  
#endif
