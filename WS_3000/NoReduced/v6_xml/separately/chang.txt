Hgamma ws
<Syst OldName = "ATLAS_lumiAux_pdf(ATLAS_lumiAux, ATLAS_lumi)"     NewName =    "ATLAS_LUMI_1517_UNCOR" />
<Syst OldName = "ATLAS_lhcMassAux_pdf(ATLAS_lhcMassAux, ATLAS_lhcMass)"     NewName =    "ATLAS_lhcMass" />
<Syst OldName = "ATLAS_spurious_ggH_0J_CENAux_pdf(ATLAS_spurious_ggH_0J_CENAux, ATLAS_spurious_ggH_0J_CEN)"     NewName =    "ATLAS_Hgg_Bias_ggH_0J_CEN" />
 <Syst OldName = "ATLAS_JET_EtaIntercalibration_TotalStatAux_pdf(ATLAS_JET_EtaIntercalibration_TotalStatAux, ATLAS_JET_EtaIntercalibration_TotalStat)"     NewName =    "ATLAS_JES_EtaIntercalibration_TotalStat_Rel21" />
<Syst OldName = "ATLAS_JET_EffectiveNP_2Aux_pdf(ATLAS_JET_EffectiveNP_2Aux, ATLAS_JET_EffectiveNP_2)"     NewName =    "ATLAS_JES_EffectiveNP_2" />
<Syst OldName = "c1_ggH_0J_CENAux_pdf(c1_ggH_0J_CENAux, c1_ggH_0J_CEN)"     NewName =    "unconstr_BGshape_p0_ggH_0J_CEN" />
<Syst OldName = "c2_ggH_0J_CENAux_pdf(c2_ggH_0J_CENAux, c2_ggH_0J_CEN)"     NewName =    "unconstr_BGshape_p1_ggH_0J_CEN" />
<Syst OldName = "nBackground_ggH_0J_CENAux_pdf(nBackground_ggH_0J_CENAux, nBackground_ggH_0J_CEN)"     NewName =    "unconstr_nbkg_Hgg_ggH_0J_CEN" />
<Syst OldName = "ATLAS_spurious_ggH_0J_FWDAux_pdf(ATLAS_spurious_ggH_0J_FWDAux, ATLAS_spurious_ggH_0J_FWD)"     NewName =    "ATLAS_Hgg_Bias_ggH_0J_FWD" />
<Syst OldName = "c1_ggH_0J_FWDAux_pdf(c1_ggH_0J_FWDAux, c1_ggH_0J_FWD)"     NewName =    "unconstr_BGshape_p0_ggH_0J_FWD" />
<Syst OldName = "c2_ggH_0J_FWDAux_pdf(c2_ggH_0J_FWDAux, c2_ggH_0J_FWD)"     NewName =    "unconstr_BGshape_p1_ggH_0J_FWD" />
<Syst OldName = "nBackground_ggH_0J_FWDAux_pdf(nBackground_ggH_0J_FWDAux, nBackground_ggH_0J_FWD)"     NewName =    "unconstr_nbkg_Hgg_ggH_0J_FWD" />
<Syst OldName = "ATLAS_spurious_ggH_1J_LOWAux_pdf(ATLAS_spurious_ggH_1J_LOWAux, ATLAS_spurious_ggH_1J_LOW)"     NewName =    "ATLAS_Hgg_Bias_ggH_1J_LOW" />
<Syst OldName = "c1_ggH_1J_LOWAux_pdf(c1_ggH_1J_LOWAux, c1_ggH_1J_LOW)"     NewName =    "unconstr_BGshape_p0_ggH_1J_LOW" />
<Syst OldName = "c2_ggH_1J_LOWAux_pdf(c2_ggH_1J_LOWAux, c2_ggH_1J_LOW)"     NewName =    "unconstr_BGshape_p1_ggH_1J_LOW" />
<Syst OldName = "nBackground_ggH_1J_LOWAux_pdf(nBackground_ggH_1J_LOWAux, nBackground_ggH_1J_LOW)"     NewName =    "unconstr_nbkg_Hgg_ggH_1J_LOW" />
<Syst OldName = "ATLAS_spurious_ggH_1J_MEDAux_pdf(ATLAS_spurious_ggH_1J_MEDAux, ATLAS_spurious_ggH_1J_MED)"     NewName =    "ATLAS_Hgg_Bias_ggH_1J_MED" />
<Syst OldName = "c1_ggH_1J_MEDAux_pdf(c1_ggH_1J_MEDAux, c1_ggH_1J_MED)"     NewName =    "unconstr_BGshape_p0_ggH_1J_MED" />
<Syst OldName = "c2_ggH_1J_MEDAux_pdf(c2_ggH_1J_MEDAux, c2_ggH_1J_MED)"     NewName =    "unconstr_BGshape_p1_ggH_1J_MED" />
<Syst OldName = "nBackground_ggH_1J_MEDAux_pdf(nBackground_ggH_1J_MEDAux, nBackground_ggH_1J_MED)"     NewName =    "unconstr_nbkg_Hgg_ggH_1J_MED" />
<Syst OldName = "ATLAS_spurious_ggH_1J_HIGHAux_pdf(ATLAS_spurious_ggH_1J_HIGHAux, ATLAS_spurious_ggH_1J_HIGH)"     NewName =    "ATLAS_Hgg_Bias_ggH_1J_HIGH" />
<Syst OldName = "c1_ggH_1J_HIGHAux_pdf(c1_ggH_1J_HIGHAux, c1_ggH_1J_HIGH)"     NewName =    "unconstr_BGshape_p0_ggH_1J_HIGH" />
<Syst OldName = "nBackground_ggH_1J_HIGHAux_pdf(nBackground_ggH_1J_HIGHAux, nBackground_ggH_1J_HIGH)"     NewName =    "unconstr_nbkg_Hgg_ggH_1J_HIGH" />
<Syst OldName = "ATLAS_spurious_ggH_1J_BSMAux_pdf(ATLAS_spurious_ggH_1J_BSMAux, ATLAS_spurious_ggH_1J_BSM)"     NewName =    "ATLAS_Hgg_Bias_ggH_1J_BSM" />
        <Syst OldName = "ATLAS_spurious_ggH_1J_BSMAux_pdf(ATLAS_spurious_ggH_1J_BSMAux, ATLAS_spurious_ggH_1J_BSM)"     NewName =    "ATLAS_Hgg_Bias_ggH_1J_BSM" />
        <Syst OldName = "c1_ggH_1J_BSMAux_pdf(c1_ggH_1J_BSMAux, c1_ggH_1J_BSM)"     NewName =    "unconstr_BGshape_p0_ggH_1J_BSM" />
        <Syst OldName = "nBackground_ggH_1J_BSMAux_pdf(nBackground_ggH_1J_BSMAux, nBackground_ggH_1J_BSM)"     NewName =    "unconstr_nbkg_Hgg_ggH_1J_BSM" />
        <Syst OldName = "c1_ggH_2J_LOWAux_pdf(c1_ggH_2J_LOWAux, c1_ggH_2J_LOW)"     NewName =    "unconstr_BGshape_p0_ggH_2J_LOW" />
        <Syst OldName = "c2_ggH_2J_LOWAux_pdf(c2_ggH_2J_LOWAux, c2_ggH_2J_LOW)"     NewName =    "unconstr_BGshape_p1_ggH_2J_LOW" />
        <Syst OldName = "nBackground_ggH_2J_LOWAux_pdf(nBackground_ggH_2J_LOWAux, nBackground_ggH_2J_LOW)"     NewName =    "unconstr_nbkg_Hgg_ggH_2J_LOW" />
        <Syst OldName = "ATLAS_spurious_ggH_2J_HIGHAux_pdf(ATLAS_spurious_ggH_2J_HIGHAux, ATLAS_spurious_ggH_2J_HIGH)"     NewName =    "ATLAS_Hgg_Bias_ggH_2J_HIGH" />
        <Syst OldName = "c1_ggH_2J_HIGHAux_pdf(c1_ggH_2J_HIGHAux, c1_ggH_2J_HIGH)"     NewName =    "unconstr_BGshape_p0_ggH_2J_HIGH" />
        <Syst OldName = "nBackground_ggH_2J_HIGHAux_pdf(nBackground_ggH_2J_HIGHAux, nBackground_ggH_2J_HIGH)"     NewName =    "unconstr_nbkg_Hgg_ggH_2J_HIGH" />
        <Syst OldName = "ATLAS_spurious_ggH_2J_HIGHAux_pdf(ATLAS_spurious_ggH_2J_HIGHAux, ATLAS_spurious_ggH_2J_HIGH)"     NewName =    "ATLAS_Hgg_Bias_ggH_2J_HIGH" />
        <Syst OldName = "c1_ggH_2J_HIGHAux_pdf(c1_ggH_2J_HIGHAux, c1_ggH_2J_HIGH)"     NewName =    "unconstr_BGshape_p0_ggH_2J_HIGH" />
        <Syst OldName = "nBackground_ggH_2J_HIGHAux_pdf(nBackground_ggH_2J_HIGHAux, nBackground_ggH_2J_HIGH)"     NewName =    "unconstr_nbkg_Hgg_ggH_2J_HIGH" />
        <Syst OldName = "ATLAS_spurious_ggH_2J_BSMAux_pdf(ATLAS_spurious_ggH_2J_BSMAux, ATLAS_spurious_ggH_2J_BSM)"     NewName =    "ATLAS_Hgg_Bias_ggH_2J_BSM" />
        <Syst OldName = "c1_ggH_2J_BSMAux_pdf(c1_ggH_2J_BSMAux, c1_ggH_2J_BSM)"     NewName =    "unconstr_BGshape_p0_ggH_2J_BSM" />
        <Syst OldName = "nBackground_ggH_2J_BSMAux_pdf(nBackground_ggH_2J_BSMAux, nBackground_ggH_2J_BSM)"     NewName =    "unconstr_nbkg_Hgg_ggH_2J_BSM" />
<Syst OldName = "ATLAS_spurious_VBF_HjjLOW_looseAux_pdf(ATLAS_spurious_VBF_HjjLOW_looseAux, ATLAS_spurious_VBF_HjjLOW_loose)"     NewName =    "ATLAS_Hgg_Bias_VBF_HjjLOW_loose" />
        <Syst OldName = "c1_VHhad_looseAux_pdf(c1_VHhad_looseAux, c1_VHhad_loose)"     NewName =    "unconstr_BGshape_p0_VHhad_loose" />
        <Syst OldName = "nBackground_VHhad_looseAux_pdf(nBackground_VHhad_looseAux, nBackground_VHhad_loose)"     NewName =    "unconstr_nbkg_Hgg_VHhad_loose" />
        <Syst OldName = "ATLAS_spurious_VHhad_tightAux_pdf(ATLAS_spurious_VHhad_tightAux, ATLAS_spurious_VHhad_tight)"     NewName =    "ATLAS_Hgg_Bias_VHhad_tight" />
        <Syst OldName = "c1_VHhad_tightAux_pdf(c1_VHhad_tightAux, c1_VHhad_tight)"     NewName =    "unconstr_BGshape_p0_VHhad_tight" />
        <Syst OldName = "nBackground_VHhad_tightAux_pdf(nBackground_VHhad_tightAux, nBackground_VHhad_tight)"     NewName =    "unconstr_nbkg_Hgg_VHhad_tight" />
        <Syst OldName = "ATLAS_spurious_qqH_BSMAux_pdf(ATLAS_spurious_qqH_BSMAux, ATLAS_spurious_qqH_BSM)"     NewName =    "ATLAS_Hgg_Bias_qqH_BSM" />
        <Syst OldName = "c1_qqH_BSMAux_pdf(c1_qqH_BSMAux, c1_qqH_BSM)"     NewName =    "unconstr_BGshape_p0_qqH_BSM" />
        <Syst OldName = "nBackground_qqH_BSMAux_pdf(nBackground_qqH_BSMAux, nBackground_qqH_BSM)"     NewName =    "unconstr_nbkg_Hgg_qqH_BSM" />
        <Syst OldName = "ATLAS_spurious_VHMET_HIGHAux_pdf(ATLAS_spurious_VHMET_HIGHAux, ATLAS_spurious_VHMET_HIGH)"     NewName =    "ATLAS_Hgg_Bias_VHMET_HIGH" />
        <Syst OldName = "c1_VHMET_HIGHAux_pdf(c1_VHMET_HIGHAux, c1_VHMET_HIGH)"     NewName =    "unconstr_BGshape_p0_VHMET_HIGH" />
        <Syst OldName = "nBackground_VHMET_HIGHAux_pdf(nBackground_VHMET_HIGHAux, nBackground_VHMET_HIGH)"     NewName =    "unconstr_nbkg_Hgg_VHMET_HIGH" />
        <Syst OldName = "c1_VHlep_LOWAux_pdf(c1_VHlep_LOWAux, c1_VHlep_LOW)"     NewName =    "unconstr_BGshape_p0_VHlep_LOW" />
        <Syst OldName = "nBackground_VHlep_LOWAux_pdf(nBackground_VHlep_LOWAux, nBackground_VHlep_LOW)"     NewName =    "unconstr_nbkg_Hgg_VHlep_LOW" />
        <Syst OldName = "ATLAS_spurious_VHlep_LOWAux_pdf(ATLAS_spurious_VHlep_LOWAux, ATLAS_spurious_VHlep_LOW)"     NewName =    "ATLAS_Hgg_Bias_VHlep_LOW" />
<Syst OldName = "ATLAS_spurious_VHlep_HIGHAux_pdf(ATLAS_spurious_VHlep_HIGHAux, ATLAS_spurious_VHlep_HIGH)"     NewName =    "ATLAS_Hgg_Bias_VHlep_HIGH" />
<Syst OldName = "c1_VHlep_HIGHAux_pdf(c1_VHlep_HIGHAux, c1_VHlep_HIGH)"     NewName =    "unconstr_BGshape_p0_VHlep_HIGH" />
 <Syst OldName = "nBackground_VHlep_HIGHAux_pdf(nBackground_VHlep_HIGHAux, nBackground_VHlep_HIGH)"     NewName =    "unconstr_nbkg_Hgg_VHlep_HIGH" />
<Syst OldName = "ATLAS_spurious_VHdilepAux_pdf(ATLAS_spurious_VHdilepAux, ATLAS_spurious_VHdilep)"     NewName =    "ATLAS_Hgg_Bias_VHdilep" />
<Syst OldName = "c1_VHdilepAux_pdf(c1_VHdilepAux, c1_VHdilep)"     NewName =    "unconstr_BGshape_p0_VHdilep" />
<Syst OldName = "nBackground_VHdilepAux_pdf(nBackground_VHdilepAux, nBackground_VHdilep)"     NewName =    "unconstr_nbkg_Hgg_VHdilep" />
 <Syst OldName = "ATLAS_spurious_ttHHad_XGBoost4Aux_pdf(ATLAS_spurious_ttHHad_XGBoost4Aux, ATLAS_spurious_ttHHad_XGBoost4)"     NewName =    "ATLAS_Hgg_Bias_ttHHad_XGBoost4" />
        <Syst OldName = "c1_ttHHad_XGBoost4Aux_pdf(c1_ttHHad_XGBoost4Aux, c1_ttHHad_XGBoost4)"     NewName =    "unconstr_BGshape_p0_ttHHad_XGBoost4" />
        <Syst OldName = "nBackground_ttHHad_XGBoost4Aux_pdf(nBackground_ttHHad_XGBoost4Aux, nBackground_ttHHad_XGBoost4)"     NewName =    "unconstr_nbkg_Hgg_ttHHad_XGBoost4" />
        <Syst OldName = "ATLAS_spurious_ttHHad_XGBoost3Aux_pdf(ATLAS_spurious_ttHHad_XGBoost3Aux, ATLAS_spurious_ttHHad_XGBoost3)"     NewName =    "ATLAS_Hgg_Bias_ttHHad_XGBoost3" />
        <Syst OldName = "c1_ttHHad_XGBoost3Aux_pdf(c1_ttHHad_XGBoost3Aux, c1_ttHHad_XGBoost3)"     NewName =    "unconstr_BGshape_p0_ttHHad_XGBoost3" />
        <Syst OldName = "nBackground_ttHHad_XGBoost3Aux_pdf(nBackground_ttHHad_XGBoost3Aux, nBackground_ttHHad_XGBoost3)"     NewName =    "unconstr_nbkg_Hgg_ttHHad_XGBoost3" />
        <Syst OldName = "ATLAS_spurious_ttHHad_XGBoost2Aux_pdf(ATLAS_spurious_ttHHad_XGBoost2Aux, ATLAS_spurious_ttHHad_XGBoost2)"     NewName =    "ATLAS_Hgg_Bias_ttHHad_XGBoost2" />
        <Syst OldName = "c1_ttHHad_XGBoost2Aux_pdf(c1_ttHHad_XGBoost2Aux, c1_ttHHad_XGBoost2)"     NewName =    "unconstr_BGshape_p0_ttHHad_XGBoost2" />
        <Syst OldName = "nBackground_ttHHad_XGBoost2Aux_pdf(nBackground_ttHHad_XGBoost2Aux, nBackground_ttHHad_XGBoost2)"     NewName =    "unconstr_nbkg_Hgg_ttHHad_XGBoost2" />
        <Syst OldName = "ATLAS_spurious_ttHHad_XGBoost1Aux_pdf(ATLAS_spurious_ttHHad_XGBoost1Aux, ATLAS_spurious_ttHHad_XGBoost1)"     NewName =    "ATLAS_Hgg_Bias_ttHHad_XGBoost1" />
        <Syst OldName = "c1_ttHHad_XGBoost1Aux_pdf(c1_ttHHad_XGBoost1Aux, c1_ttHHad_XGBoost1)"     NewName =    "unconstr_BGshape_p0_ttHHad_XGBoost1" />
        <Syst OldName = "nBackground_ttHHad_XGBoost1Aux_pdf(nBackground_ttHHad_XGBoost1Aux, nBackground_ttHHad_XGBoost1)"     NewName =    "unconstr_nbkg_Hgg_ttHHad_XGBoost1" />
        <Syst OldName = "ATLAS_spurious_ttHLep_XGBoost3Aux_pdf(ATLAS_spurious_ttHLep_XGBoost3Aux, ATLAS_spurious_ttHLep_XGBoost3)"     NewName =    "ATLAS_Hgg_Bias_ttHLep_XGBoost3" />
        <Syst OldName = "c1_ttHLep_XGBoost3Aux_pdf(c1_ttHLep_XGBoost3Aux, c1_ttHLep_XGBoost3)"     NewName =    "unconstr_BGshape_p0_ttHLep_XGBoost3" />
        <Syst OldName = "nBackground_ttHLep_XGBoost3Aux_pdf(nBackground_ttHLep_XGBoost3Aux, nBackground_ttHLep_XGBoost3)"     NewName =    "unconstr_nbkg_Hgg_ttHLep_XGBoost3" />
        <Syst OldName = "ATLAS_spurious_ttHLep_XGBoost2Aux_pdf(ATLAS_spurious_ttHLep_XGBoost2Aux, ATLAS_spurious_ttHLep_XGBoost2)"     NewName =    "ATLAS_Hgg_Bias_ttHLep_XGBoost2" />
        <Syst OldName = "c1_ttHLep_XGBoost2Aux_pdf(c1_ttHLep_XGBoost2Aux, c1_ttHLep_XGBoost2)"     NewName =    "unconstr_BGshape_p0_ttHLep_XGBoost2" />
        <Syst OldName = "nBackground_ttHLep_XGBoost2Aux_pdf(nBackground_ttHLep_XGBoost2Aux, nBackground_ttHLep_XGBoost2)"     NewName =    "unconstr_nbkg_Hgg_ttHLep_XGBoost2" />
        <Syst OldName = "ATLAS_spurious_ttHLep_XGBoost1Aux_pdf(ATLAS_spurious_ttHLep_XGBoost1Aux, ATLAS_spurious_ttHLep_XGBoost1)"     NewName =    "ATLAS_Hgg_Bias_ttHLep_XGBoost1" />
        <Syst OldName = "c1_ttHLep_XGBoost1Aux_pdf(c1_ttHLep_XGBoost1Aux, c1_ttHLep_XGBoost1)"     NewName =    "unconstr_BGshape_p0_ttHLep_XGBoost1" />
        <Syst OldName = "nBackground_ttHLep_XGBoost1Aux_pdf(nBackground_ttHLep_XGBoost1Aux, nBackground_ttHLep_XGBoost1)"     NewName =    "unconstr_nbkg_Hgg_ttHLep_XGBoost1" />



donot have:
<Syst OldName = "ATLAS_FT_EFF_Eigen_B_1Aux_pdf(ATLAS_FT_EFF_Eigen_B_1Aux, ATLAS_FT_EFF_Eigen_B_1)"     NewName =    "ATLAS_FT_EFF_Eigen_B_1_Rel21_channel_HGam" />











HWW ws
<Syst OldName = "theo_NonWW_matching_scaleConstraint(theo_NonWW_matching_scale, nom_alpha_theo_NonWW_matching_scale)"     NewName =    "BkgTheory_MatchedScale_VV_channel_HWW" />
        <Syst OldName = "theo_VBF_pdf_ggConstraint(theo_VBF_pdf_gg, nom_alpha_theo_VBF_pdf_gg)"     NewName =    "TheorySig_PDF_VBF" />
<Syst OldName = "theo_VBF_qcd_scale_ggConstraint(theo_VBF_qcd_scale_gg, nom_alpha_theo_VBF_qcd_scale_gg)"     NewName =    "TheorySig_QCDscale_VBF" />
 <Syst OldName = "theo_ggf_QCDscale_ggHConstraint(theo_ggf_QCDscale_ggH, nom_alpha_theo_ggf_QCDscale_ggH)"     NewName =    "TheorySig_QCDscale_ggF_mu" />
<Syst OldName = "theo_ggf_pdf_ggConstraint(theo_ggf_pdf_gg, nom_alpha_theo_ggf_pdf_gg)"     NewName =    "TheorySig_PDF_ggF" />








Hgamma mu ws compared to xs workspace:
do not have:
<Syst OldName = "ATLAS_PDF4LHC_NLO_30_EV30Aux_pdf(ATLAS_PDF4LHC_NLO_30_EV30Aux, ATLAS_PDF4LHC_NLO_30_EV30)"     NewName =    "" />
<Syst OldName = "ATLAS_PDF4LHC_NLO_30_EV13Aux_pdf(ATLAS_PDF4LHC_NLO_30_EV13Aux, ATLAS_PDF4LHC_NLO_30_EV13)"     NewName =    "" />
<Syst OldName = "ATLAS_PDF4LHC_NLO_30_EV3Aux_pdf(ATLAS_PDF4LHC_NLO_30_EV3Aux, ATLAS_PDF4LHC_NLO_30_EV3)"     NewName =    "" />
<Syst OldName = "ATLAS_PDF4LHC_NLO_30_EV29Aux_pdf(ATLAS_PDF4LHC_NLO_30_EV29Aux, ATLAS_PDF4LHC_NLO_30_EV29)"     NewName =    "" />
<Syst OldName = "ATLAS_PDF4LHC_NLO_30_EV23Aux_pdf(ATLAS_PDF4LHC_NLO_30_EV23Aux, ATLAS_PDF4LHC_NLO_30_EV23)"     NewName =    "" />
<Syst OldName = "ATLAS_PDF4LHC_NLO_30_EV25Aux_pdf(ATLAS_PDF4LHC_NLO_30_EV25Aux, ATLAS_PDF4LHC_NLO_30_EV25)"     NewName =    "" />

