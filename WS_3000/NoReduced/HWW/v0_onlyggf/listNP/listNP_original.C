using namespace RooFit;
using namespace RooStats;
using namespace std;

void listNP(TString fileName){
  TFile*f=TFile::Open(fileName);
  RooWorkspace *w=(RooWorkspace*)f->Get("combWS");
  ModelConfig *mc=(ModelConfig*)w->obj("ModelConfig");
  RooArgSet *nuisanceParameters=(RooArgSet*)mc->GetNuisanceParameters();

  TIterator *iter = nuisanceParameters -> createIterator();
  RooRealVar* parg = NULL;

  while((parg=(RooRealVar*)iter->Next()) ){

    TString nuisName=parg->GetName();
    cout<<nuisName<<endl;
  }

  f->Close();
}
