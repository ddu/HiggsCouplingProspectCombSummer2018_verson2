using namespace RooFit;
using namespace RooStats;
using namespace std;
void string_replace(string &s1,const string &s2,const string &s3);
//void listNP(TString fileName="HWW_combined_SysOnly_125_model.root"){
void listNP(TString fileName="hwwlvlv_combined_125_SM_model.root"){
//  const char* out_file="dds_VBF";
  const char* out_file="dds_ggF";
  string JES="JES";
  string JET="JET";
//////////////////////
  string JET_JER_SINGLE_NP="JET_JER_SINGLE_NP";
  string JER="JER";
////////////////////////
  string HWW_FakeFactor_el="HWW_FakeFactor_el";
  string ATLAS_elFF="ATLAS_elFF";
/////////////////////////
  string HWW_FakeFactor_mu="HWW_FakeFactor_mu";
  string ATLAS_muFF="ATLAS_muFF";
////////////////////////
  string QCDScale_ggH="QCDScale_ggH";
  string weight_qcd_wg1_mu="weight_qcd_wg1_mu";
//////////////////////////////////////////////
  string ATLAS_QCDScale_qqH="ATLAS_QCDScale_qqH";
  string theo_VBF_qcd_scale_gg="theo_VBF_qcd_scale_gg";
//////////////////////////////////////////////
  string ATLAS_pdf_Higgs_gg="ATLAS_pdf_Higgs_gg";
  string theo_ggf_pdf_gg="theo_ggf_pdf_gg";
//////////////////////////////////////////////
  string ATLAS_pdf_Higgs_qq="ATLAS_pdf_Higgs_qq";
  string theo_VBF_pdf_gg="theo_VBF_pdf_gg";
//////////////////////////////////////////////
  string EffectiveNP_8="EffectiveNP_8";
  string EffectiveNP_8restTerm="EffectiveNP_8restTerm";
//////////////////////////////////////////////
  string JET_Flavor_Composition="JET_Flavor_Composition";
  string JET_Flavor_Composition_Top="JET_Flavor_Composition_Top";
//////////////////////
  string Pile="Pile";
  string Pileup="Pileup";
//////////////////////
  string RhoTopology="RhoTopology";
  string RhoTopology_Top="RhoTopology_Top";
//////////////////////
  string MUONS="MUONS";
  string MUON="MUON";
//////////////////////
  string ATLAS_LUMI_lumi="ATLAS_LUMI_lumi";
  string ATLAS_LUMI="ATLAS_LUMI";
//////////////////////
  system(Form("rm -f result/%s.txt", out_file));
  FILE *pinfo = fopen(Form("result/%s.txt", out_file), "w");
  
  TFile*f=TFile::Open(fileName);
  RooWorkspace *w=(RooWorkspace*)f->Get("combined");
  ModelConfig *mc=(ModelConfig*)w->obj("ModelConfig");
  RooArgSet *nuisanceParameters=(RooArgSet*)mc->GetNuisanceParameters();

  TIterator *iter = nuisanceParameters -> createIterator();
  RooRealVar* parg = NULL;

  while((parg=(RooRealVar*)iter->Next()) ){

    TString nuisName=parg->GetName();
    string nuisName_tmp=parg->GetName();
    cout<<nuisName<<endl;
    if(nuisName.Contains("alpha")){
    nuisName_tmp.erase(nuisName_tmp.begin(),nuisName_tmp.begin()+6 );
    //cout<<nuisName<<endl;
    cout<<nuisName_tmp<<endl;
    //if (nuisName.Contains("RhoTopology")&&!nuisName.Contains("Top")){string_replace(nuisName_tmp,RhoTopology,RhoTopology_Top);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("JES")){string_replace(nuisName_tmp,JES,JET);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("JER")&&!nuisName.Contains("SINGLE_NP")){string_replace(nuisName_tmp,JER,JET_JER_SINGLE_NP);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("HWW_FakeFactor_el")){string_replace(nuisName_tmp,HWW_FakeFactor_el,ATLAS_elFF);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("HWW_FakeFactor_mu")){string_replace(nuisName_tmp,HWW_FakeFactor_mu,ATLAS_muFF);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("QCDScale_ggH")){string_replace(nuisName_tmp,QCDScale_ggH,weight_qcd_wg1_mu);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("ATLAS_QCDScale_qqH")){string_replace(nuisName_tmp,ATLAS_QCDScale_qqH,theo_VBF_qcd_scale_gg);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("ATLAS_pdf_Higgs_gg")){string_replace(nuisName_tmp,ATLAS_pdf_Higgs_gg,theo_ggf_pdf_gg);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("ATLAS_pdf_Higgs_qq")){string_replace(nuisName_tmp,ATLAS_pdf_Higgs_qq,theo_VBF_pdf_gg);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("EffectiveNP_8")&&!nuisName.Contains("restTerm")){string_replace(nuisName_tmp,EffectiveNP_8,EffectiveNP_8restTerm);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("JET_Flavor_Composition")&&!nuisName.Contains("Top")){string_replace(nuisName_tmp,JET_Flavor_Composition,JET_Flavor_Composition_Top);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("Pileup")){string_replace(nuisName_tmp,Pileup,Pile);cout<<nuisName_tmp<<" \n "<<endl;}
    if ((nuisName.Contains("RhoTopology")&&!nuisName.Contains("RhoTopology_Top"))&&!nuisName.Contains("RhoTopology_NonTop")){string_replace(nuisName_tmp,RhoTopology,RhoTopology_Top);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("MUONS")){string_replace(nuisName_tmp,MUONS,MUON);cout<<nuisName_tmp<<" \n "<<endl;}
    if (nuisName.Contains("ATLAS_LUMI_lumi")){string_replace(nuisName_tmp,ATLAS_LUMI_lumi,ATLAS_LUMI);cout<<nuisName_tmp<<" \n "<<endl;}
    
    fprintf(pinfo, "<Syst OldName = \"%s\"     NewName = \"%s\" />", nuisName.Data(),nuisName_tmp.c_str() );
    fprintf(pinfo,  "\n");}
    else if(nuisName.Contains("gamma")){
    cout<<"gamma:"<<nuisName<<endl;
    fprintf(pinfo, "<Syst OldName = \"%s\"     NewName = \"%s\" />", nuisName.Data(),nuisName.Data() );
    fprintf(pinfo,  "\n");
    }
  }

  f->Close();
}
void string_replace(string &s1,const string &s2,const string &s3)
{
	string::size_type pos=0;
	string::size_type a=s2.size();
	string::size_type b=s3.size();
	while((pos=s1.find(s2,pos))!=string::npos)
	{
		s1.erase(pos,a);
		s1.insert(pos,s3);
		pos+=b;
	}
}
