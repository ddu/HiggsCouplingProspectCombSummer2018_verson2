using namespace RooFit;
using namespace RooStats;
using namespace std;
void string_replace(string &s1,const string &s2,const string &s3);
void listNP_0223(TString fileName="HWW_combined_SysOnly_125_model.root"){
//void listNP(TString fileName="hwwlvlv_combined_125_SM_model.root"){
  string JES="JES";
  string JET="JET";
  const char* out_file="dds_VBF_test";
//  const char* out_file="dds_ggF";
  system(Form("rm -f result/%s.txt", out_file));
  FILE *pinfo = fopen(Form("result/%s.txt", out_file), "w");
  
  TFile*f=TFile::Open(fileName);
  RooWorkspace *w=(RooWorkspace*)f->Get("combined");
  ModelConfig *mc=(ModelConfig*)w->obj("ModelConfig");
  RooArgSet *nuisanceParameters=(RooArgSet*)mc->GetNuisanceParameters();

  TIterator *iter = nuisanceParameters -> createIterator();
  RooRealVar* parg = NULL;

  while((parg=(RooRealVar*)iter->Next()) ){

    TString nuisName=parg->GetName();
    string nuisName_tmp=parg->GetName();
    if(nuisName.Contains("alpha")){
    nuisName_tmp.erase(nuisName_tmp.begin(),nuisName_tmp.begin()+6 );
    cout<<nuisName<<endl;
    cout<<nuisName_tmp<<endl;
    if (nuisName.Contains("JES")){string_replace(nuisName_tmp,JES,JET);cout<<" \n "<<nuisName_tmp<<endl;}
    fprintf(pinfo, "<Syst OldName = \"%s\"     NewName = \"%s\" />", nuisName.Data(),nuisName_tmp.c_str() );
    fprintf(pinfo,  "\n");}
    else if(nuisName.Contains("gamma")){
    cout<<"gamma:"<<nuisName<<endl;
    fprintf(pinfo, "<Syst OldName = \"%s\"     NewName = \"%s\" />", nuisName.Data(),nuisName.Data() );
    fprintf(pinfo,  "\n");
    }
    else
    cout<<"other:"<<nuisName<<endl;
    fprintf(pinfo, "<Syst OldName = \"%s\"     NewName = \"%s\" />", nuisName.Data(),nuisName.Data() );
    fprintf(pinfo,  "\n");
  }

  f->Close();
}
void string_replace(string &s1,const string &s2,const string &s3)
{
	string::size_type pos=0;
	string::size_type a=s2.size();
	string::size_type b=s3.size();
	while((pos=s1.find(s2,pos))!=string::npos)
	{
		s1.erase(pos,a);
		s1.insert(pos,s3);
		pos+=b;
	}
}
